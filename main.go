package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

// // entries represents data about a record album.
// type entry struct {
// 	Author  string `json:"author"`
// 	Message string `json:"message"`
// }

// // entries slice to seed record album data.
// var entries = []entry{
// 	{Author: "Blue Train", Message: "John Coltrane"},
// 	{Author: "Jeru", Message: "Gerry Mulligan"},
// 	{Author: "Sarah Vaughan and Clifford Brown", Message: "Sarah Vaughan"},
// }

func index(w http.ResponseWriter, req *http.Request) {
	t := time.Now()

	h := t.Hour()
	m := t.Minute()

	limiter := "h"

	ans := fmt.Sprintf("%d%s%d", h, limiter, m)

	fmt.Fprintf(w, ans)
}

func listEntries() []string {
	raw, err := ioutil.ReadFile("data.txt") // lire le fichier text.txt

	if err != nil {
		fmt.Println(err)
	}

	data := strings.Split(string(raw), "\n")

	return data
}

func getEntries(w http.ResponseWriter, req *http.Request) {
	entries := listEntries()

	for _, rawEntry := range entries {
		entry := strings.Split(rawEntry, ":")

		fmt.Fprintf(w, entry[1]+"\n")
	}
}

func main() {
	// router := gin.Default()
	// router.GET("/", getTime)
	// router.GET("/entries", getEntries)

	// // http.HandleFunc("/", index)
	// // http.ListenAndServe("4567", nil)
	// router.Run("localhost:4567")

	http.HandleFunc("/", index)
	http.HandleFunc("/entries", getEntries)

	fmt.Println("Server started")
	http.ListenAndServe(":4567", nil)
}
